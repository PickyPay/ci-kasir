<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Muser extends CI_Model {

    private $_table = "user";
    public $email;
    public $username;
    public $password;
    public $address;

    public function cek_login($table, $where) {
        return $this->db->get_where($table, $where);
    }

    public function rules() {
        return [
            ['field' => 'email',
            'label' => 'Email',
            'rules' => 'required'],
            
            ['field' => 'username',
            'label' => 'Username',
            'rules' => 'required'],

            ['field' => 'password',
            'label' => 'Password',
            'rules' => 'required'],

            ['field' => 'address',
            'label' => 'Address',
            'rules' => 'required']
        ];
    }

    // public function login() {
    //     $post = $this->input->post();
    //     $this->email = $post["email"];
    //     $this->password = $post["password"];
    // }

    public function register() {
        $post = $this->input->post();
        $this->email = $post["email"];
        $this->username = $post['username'];
        $this->password = $post['password'];
        $this->address = $post['address'];
        $this->db->insert($this->_table, $this);
    }

}


/* End of file Muser.php */