<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<title>Login Page</title>

<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body style="background-color: grey;">
  <h1><br></h1>
  <br>
  <section class="login-block">
    <div class="container">
      <div class="row">
        <div class="col-md-4 login-sec">
          <h2 class="text-center"><b>Login</b> <font color="green">Now</font></h2>
          <br>
          <form action="<?php echo base_url('index.php/User/action_login') ?>" method="post">
            <form class="login-form">
              <div class="form-group">

                <label>USERNAME</label>
                <input type="text" required name="username" class="form-control" id="inputUsername">

              </div>
              <div class="form-group">

                <label>PASSWORD</label>
                <input type="password" required name="password" class="form-control" id="inputPassword">

              </div>


              <div>
                <input type="submit" name="btn" value="LOGIN" class="btn btn-primary float-right">
              </div>
            </form>
            <br>
            <br>
            <div class="forgot">Dont have an account?
              <a href="<?php echo base_url('index.php/User/register') ?>"><font color="white">Register here</font></a>
            </div>
          </div>
          <div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
             <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide"  style="border-radius: 15%">
                <div class="carousel-caption d-none d-md-block">
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid" src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg" alt="First slide" style="border-radius: 15%">
                <div class="carousel-caption d-none d-md-block">
                </div>
              </div>
            </div>     

          </div>
        </div>
      </div>
    </section>
  </body>