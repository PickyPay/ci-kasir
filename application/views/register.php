<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<html>

<head>
    <link href="<?php echo base_url('assets/css/login.css') ?>" rel="stylesheet" >
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>

<body id="LoginForm">
    <div class="container"><br>
        <div class="login-form">
            <div class="main-div">
                <div class="panel">
                    <h2>Form Register</h2>
                    <p>Please enter your data to register</p>
                </div>
                <form action="<?php echo base_url('index.php/User/action_register') ?>" method="post" enctype="multipart/form-data">

                    <div class="form-group">


                        <input type="email" name="email" class="form-control <?php echo form_error('email') ? 'is-invalid':'' ?>" id="inputEmail" placeholder="Email Address">
                        <div class="invalid-feedback">	
        				<?php echo form_error('email') ?>
					</div>

                    </div>

                    <div class="form-group">

                    <input type="text" name="username" class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>" id="inputUsername" placeholder="Username">
                    <div class="invalid-feedback">	
        				<?php echo form_error('username') ?>
					</div>
                    </div>

                    <div class="form-group">

                        <input type="password" name="password" class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>" id="inputPassword" placeholder="Password">
                        <div class="invalid-feedback">	
        				<?php echo form_error('password') ?>
					</div>
                    </div>

                    <div class="form-group">

                        <input type="text" name="address" class="form-control <?php echo form_error('address') ? 'is-invalid':'' ?>" id="inputAddress" placeholder="Your Address">
                        <div class="invalid-feedback">	
        				<?php echo form_error('address') ?>
					</div>
                    </div>

                     <input type="submit" name="btn" value="REGISTER" class="btn btn-primary" /><br>
                     <div class="forgot">
                        <a href="<?php echo base_url('index.php/User') ?>">back to login</a>
                    </div>
            

                </form>
            </div>
            
        </div>
    </div>
    </div>


</body>

</html>