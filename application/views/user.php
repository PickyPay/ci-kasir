<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="../image/png" href="../assets/img/favicon.ico"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>PickyMart</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- FontAwesome -->
    <link href="<?php echo base_url('assets/fontawesome/css/fontawesome.css') ?>" rel="stylesheet" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css?v=1.4.0') ?>" rel="stylesheet" />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet" />
</head>

<body>

    <div class="wrapper">
        <div class="sidebar">

            <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


            <div class="sidebar-wrapper" style="background-color: black;">
                <div class="logo"  style="background-color: red;">
                    <a href="<?php echo base_url('index.php/Product') ?>" class="simple-text" >
                        <font color="black">
                            <b>
                                PICKYMART
                            </b>
                        </font>
                    </a>
                </div>

                <ul class="nav">
                    <li>
                        <a href="<?php echo base_url('index.php/Product') ?>">
                            <div style="font-size: 1.5rem;"><i class="fas fa-store fa-2x"></i>&nbsp &nbspDashboard</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/add') ?>">
                            <div style="font-size: 1.5rem;">&nbsp <i class="fas fa-clipboard-list fa-2x"></i>&nbsp &nbsp &nbspTambah Barang</div>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/Product/dataTransaksi') ?>">
                            <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-chart-bar fa-2x"></i>&nbsp &nbsp Riwayat Transaksi</div>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo base_url('index.php/Product/profile') ?>">
                            <div style="font-size: 1.5rem;">&nbsp<i class="fas fa-user-edit fa-2x"></i>&nbsp User Profile</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid"  style="background-color: black;">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse">

                        <ul class="nav navbar-nav navbar-right" style="background-color: red;">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <p> <font color="black"> <i class="fas fa-user-circle"></i>  <?php echo $this->session->userdata("username"); ?> 
                                    <b class="caret"></b>
                                </font>
                            </p>

                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/User/logout') ?>">Log out</a></li>
                        </ul>
                    </li>
                    <li class="separator hidden-lg"></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                        </div>
                        <div class="content">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="<?php echo base_url('assets/img/faces/default-avatar.png') ?>"
                                    alt="..." />

                                    <h4 class="title"><?php echo $this->session->userdata("username"); ?><br />
                                        <small><?php echo $this->session->userdata("email"); ?></small>
                                    </h4>
                                </a>
                            </div>
                            <p class="description text-center">
                            </p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button href="#" class="btn btn-simple"><i class="fab fa-facebook-square fa-lg"></i></button>
                            <button href="#" class="btn btn-simple"><i class="fab fa-twitter-square fa-lg"></i></i></button>
                            <button href="#" class="btn btn-simple"><i class="fab fa-google-plus-square fa-lg"></i></i></button>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Profile</h4>
                        </div>
                        <div class="content">
                            <form>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" placeholder="Username"
                                            value="<?php echo $this->session->userdata("username"); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" placeholder="Email"
                                            value="<?php echo $this->session->userdata("email"); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" class="form-control" placeholder="Home Address"
                                            value="<?php echo $this->session->userdata("address") ?>">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <footer class="footer">
        <div class="container-fluid">
            <p class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script> <a href="#">Ayzar Fachru M</a>, make a better web.
            </p>
        </div>
    </footer>

</div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
  </div>
  <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
  <div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    <a class="btn btn-primary" href="<?php echo base_url('user/logout'); ?>">Logout</a>
</div>
</div>
</div>
</div>

</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url('assets/js/jquery.3.2.1.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url('assets/js/chartist.min.js') ?>"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0') ?>"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>

<script src="<?php echo base_url('assets/js/all.min.js') ?>" type="text/javascript"></script>

</html>