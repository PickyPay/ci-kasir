<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransaksi extends CI_Model {

    private $_table = "transaksi";
    public $id_transaksi;
    public $id_product;
    public $name;
    public $price;
    public $stok;
    public $total;

    public function rules() {
        return [
            ['field' => 'stok',
            'label' => 'Stok',
            'rules' => 'required' ]
        ];
    }

    public function getAll() {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id) {
        return $this->db->get_where($this->_table, ["id_transaksi" => $id])->row();
    }

    public function save() {
        $post = $this->input->post();
        $this->id_product = $post["id"];
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->stok = $post["stok1"];
        $this->total = $post["total"];
        $this->db->insert($this->_table, $this);
    }

    public function delete($id) {
        return $this->db->delete($this->_table, array("id_transaksi" => $id));
    }

    public function getKeyword($keyword) {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->like('name', $keyword);
        $this->db->or_like('price', $keyword);
        $this->db->or_like('stok', $keyword);
        $this->db->or_like('total', $keyword);
        $this->db->or_like('history', $keyword);
        return $this->db->get()->result();
    }
}

/* End of file Mtransaksi.php */
