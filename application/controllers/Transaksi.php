<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mtransaksi');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->load->view('transaksi');
    }

}

/* End of file Transaksi.php */
