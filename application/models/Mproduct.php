<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Mproduct extends CI_Model {

    private $_table = "products";
    public $id_product;
    public $name;
    public $price;
    public $stok;

    public function rules() {
        return [
            ['field' => 'name',
            'label' => 'Name',
            'rules' => 'required'],

            ['field' => 'price',
            'label' => 'Price',
            'rules' => 'required'],

            ['field' => 'stok',
            'label' => 'Stok',
            'rules' => 'required']
        ];
    }

    public function getAll() {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id) {
        return $this->db->get_where($this->_table, ["id_product" => $id])->row();
    }

    public function save() {
        $post = $this->input->post();
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->stok = $post["stok"];
        $this->db->insert($this->_table, $this);
    }

    public function update() {
        $post = $this->input->post();
        $this->id_product = $post["id"];
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->stok = $post["stok"];
        $this->db->update($this->_table, $this, array('id_product' => $post['id'] ));
    }

    public function delete($id) {
        return $this->db->delete($this->_table, array("id_product" => $id));
    }

    public function getKeyword($keyword) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->like('name', $keyword);
        $this->db->or_like('price', $keyword);
        $this->db->or_like('stok', $keyword);
        return $this->db->get()->result();
    }

}

/* End of file ModelName.php */
