<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("mproduct");
        $this->load->model("muser");
        $this->load->model('mtransaksi');
        $this->load->library('form_validation');
    }

    public function index() {
        // $cek = $this->muser->security();
        // if(!empty())
        $data["products"] = $this->mproduct->getAll();
        $this->load->view("dashboard", $data);
    }

    public function add() {
        $product = $this->mproduct;
        $validation = $this->form_validation;
        $validation->set_rules($product->rules());

        if ($validation->run()) {
            $product->save();
            // $this->session->set_flashdata('succes', 'berhasil disimpan');
            redirect(base_url('index.php/Product/add'));
        }

        $this->load->view("add");
    }

    public function profile() {
        $this->load->view("user");
    }

    public function dataTransaksi() {
        $data["transaksi"] = $this->mtransaksi->getAll();
        $this->load->view("data_transaksi", $data);
    }

    public function edit($id = null) {
        if (!isset($id)) redirect (base_url('index.php/Product'));
        $product = $this->mproduct;
        $validation = $this->form_validation;
        $validation->set_rules($product->rules());

        if ($validation->run()) {
            $product->update();
            redirect (base_url('index.php/Product'));
        }

        $data["product"] = $product->getById($id);
        if (!$data["product"]) redirect(base_url());

        $this->load->view("edit", $data);

    }

    public function delete($id = null) {

        if (!isset($id)) show_404();

        if ($this->mproduct->delete($id)) {
            redirect(base_url('index.php/Product'));
        }

    }

    public function deleteTransaksi($id = null) {
        if (!isset($id)) show_404();

        if ($this->mtransaksi->delete($id)) {
            redirect(base_url('index.php/Product/dataTransaksi'));
        }
    }

    public function transaksi($id = null) {
        if (!isset($id)) redirect (base_url('index.php/Product'));
        $product = $this->mproduct;
        $transaksi = $this->mtransaksi;
        $validation = $this->form_validation;
        $validation->set_rules($transaksi->rules());

        if ($validation->run()) {
            $transaksi->save();
            redirect (base_url('index.php/Product'));
        }
        $data["product"] = $product->getById($id);
        if (!$data["product"]) redirect(base_url('index.php/Product'));
        $this->load->view('transaksi', $data);
    }

    public function searchProduct() {
        $keyword = $this->input->post("keyword");
        $data["products"] = $this->mproduct->getKeyword($keyword);
        $this->load->view("search_product", $data);
    }

    public function searchTransaksi() {
        $keyword = $this->input->post("keyword");
        $data["transaksi"] = $this->mtransaksi->getKeyword($keyword);
        $this->load->view("search_transaksi", $data);
    }

}

/* End of file Controllername.php */